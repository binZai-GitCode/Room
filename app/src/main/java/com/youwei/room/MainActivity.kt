package com.youwei.room

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.youwei.room.adapter.ListAdapter
import com.youwei.room.dao.ClassesDao
import com.youwei.room.database.SchoolDatabase
import com.youwei.room.databinding.ActivityMainBinding
import com.youwei.room.entity.Classes
import com.youwei.room.entity.Students
import com.youwei.room.ui.Popup
import java.util.*

class MainActivity : AppCompatActivity() {
    val add = Arrays.asList("添加一个班级", "添加多个班级")
    val delete = Arrays.asList("删除一条数据", "删除多条数据")
    val update = Arrays.asList("修改一条数据", "修改多条数据")
    var db: SchoolDatabase? = null
    var list: MutableList<Classes>? = mutableListOf()
    var listAdapter: ListAdapter? = null

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        System.loadLibrary("sqlcipher");
        val databasePath = getDatabasePath("school.db")
        databasePath.mkdirs()
        // 初始化数据库
        db = SchoolDatabase.getDataBase(this)

        binding.recycle.layoutManager = LinearLayoutManager(this)
        updateData()
        listAdapter = ListAdapter(this, list)
        binding.recycle.adapter = listAdapter

        /**
         * 各种对数据库的操作
         */
        Popup(this).bind(binding.add, add) { position ->
            Thread(Runnable {
                val allClasses = db?.classDao()?.getAllClasses()
                if (position == 0) {
                    var classes = Classes(0, allClasses?.size.toString() + "班","男");
                    db?.classDao()?.addClasses(classes)
                } else {
                    var classes1 = Classes(0, allClasses?.size.toString() + "班","男");
                    var classes2 = Classes(0, allClasses?.size.toString() + "班","男");
                    var classes3 = Classes(0, allClasses?.size.toString() + "班","男");
                    var classes4 = Classes(0, allClasses?.size.toString() + "班","男");
                    db?.classDao()?.addClasses(classes1, classes2, classes3, classes4)
                }
                updateData()
            }).start()
        }

        Popup(this).bind(binding.delete, delete) { position ->
            Thread(Runnable {
                val allClasses = db?.classDao()?.getAllClasses()
                if (position == 0) {
                    if (allClasses!!.size > 0) {
                        db?.classDao()?.deleteClasses(allClasses!![allClasses!!.size - 1])
                    }
                } else {
                    if (allClasses!!.size > 3) {
                        db?.classDao()?.deleteClasses(
                            allClasses!![allClasses!!.size - 4],
                            allClasses!![allClasses!!.size - 3],
                            allClasses!![allClasses!!.size - 2],
                            allClasses!![allClasses!!.size - 1]
                        )
                    } else if (allClasses!!.size > 2) {
                        db?.classDao()?.deleteClasses(
                            allClasses!![allClasses!!.size - 3],
                            allClasses!![allClasses!!.size - 2],
                            allClasses!![allClasses!!.size - 1]
                        )
                    } else if (allClasses!!.size > 1) {
                        db?.classDao()?.deleteClasses(
                            allClasses!![allClasses!!.size - 2],
                            allClasses!![allClasses!!.size - 1]
                        )
                    } else if (allClasses!!.size > 0) {
                        db?.classDao()?.deleteClasses(allClasses!![allClasses!!.size - 1])
                    }
                }
                updateData()
            }).start()
        }


        Popup(this).bind(binding.update, update) { position ->
            Thread(Runnable {
                val allClasses = db?.classDao()?.getAllClasses()
                if (allClasses?.size!! > 0) {
                    if (position == 0) {
                        // 修改一条数据
                        var classes = allClasses!![allClasses?.size - 1]
                        classes.classesName = "班级已修改"
                        db?.classDao()?.updateClasses(classes)
                    } else {
                        var classes1 = allClasses!![allClasses?.size - 1]
                        classes1.classesName = "班级已修改"
                        if (allClasses?.size - 2 > 0) {
                            var classes2 = allClasses!![allClasses?.size - 2]
                            classes2.classesName = "班级已修改"
                            if (allClasses?.size - 3 > 0) {
                                var classes3 = allClasses!![allClasses?.size - 3]
                                classes3.classesName = "班级已修改"
                                if (allClasses?.size - 4 > 0) {
                                    var classes4 = allClasses!![allClasses?.size - 4]
                                    classes4.classesName = "班级已修改"
                                    db?.classDao()
                                        ?.updateClasses(classes1, classes2, classes3, classes4)
                                }
                                db?.classDao()?.updateClasses(classes1, classes2, classes3)
                            }
                            db?.classDao()?.updateClasses(classes1, classes2)
                        }
                        db?.classDao()?.updateClasses(classes1)
                    }
                    updateData()
                } else {
                    Snackbar.make(binding.update, "无数据可修改", Snackbar.LENGTH_LONG).show()
                }
            }).start()
        }

        // 查询数据
        binding.search.setOnClickListener {
            updateData()
        }

    }

    private fun updateData() {
        Thread(
            Runnable {
                list?.clear()
                list?.addAll(db?.classDao()!!.getAllClasses())
                runOnUiThread {
                    listAdapter?.notifyDataSetChanged()
                }
            }
        ).start()
    }

    override fun onDestroy() {
        super.onDestroy()
        db?.close()
    }
}