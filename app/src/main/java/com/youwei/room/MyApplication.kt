package com.youwei.room

import android.app.Application
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.youwei.room.dao.ClassesDao
import com.youwei.room.dao.StudentsDao
import com.youwei.room.database.SchoolDatabase
import com.youwei.room.entity.Classes
import com.youwei.room.entity.Students

/**
 * @Author：Bin YouWei
 * @Date：2022/6/27/14:17
 * @Declaration：
 */
class MyApplication : Application() {

}