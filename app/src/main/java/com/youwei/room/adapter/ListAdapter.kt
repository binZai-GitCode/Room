package com.youwei.room.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.youwei.room.R
import com.youwei.room.entity.Classes

/**
 * @Author：Bin YouWei
 * @Date：2022/7/1917:18
 * @CSDN：https://blog.csdn.net/baidu_41616022
 * @Declaration：
 */
class ListAdapter(context: Context, list: List<Classes>?) :
    RecyclerView.Adapter<ListAdapter.MyViewHolder>() {
    var context: Context
    var list: List<Classes>?

    init {
        this.context = context
        this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflate = LayoutInflater.from(parent.context)
            .inflate(android.R.layout.simple_list_item_1, parent, false)
        return MyViewHolder(inflate)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.text1.text = list!![position].classesName
    }

    override fun getItemCount(): Int {
        if (list == null) {
            return 0
        } else {
            return list!!.size
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var text1: TextView

        init {
            text1 = itemView.findViewById(android.R.id.text1)
        }
    }
}