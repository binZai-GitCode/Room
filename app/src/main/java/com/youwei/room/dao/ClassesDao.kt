package com.youwei.room.dao

import androidx.room.*
import com.youwei.room.entity.Classes

/**
 * @Author：Bin YouWei
 * @Date：2022/6/27/11:51
 * @Declaration：用于实现对Class表的数据查询
 */
@Dao
interface ClassesDao {

    /**
     * 增加一个或多个班级
     */
    @Insert
    fun addClasses(vararg classes: Classes)

    @Delete
    fun deleteClasses(vararg classes: Classes)

    @Update
    fun updateClasses(vararg classes: Classes)

    /**
     * 查询classes表的所有数据
     */
    @Query("select * from classes")
    fun getAllClasses() : List<Classes>

    /**
     * 查询classes表的某一条数据
     */
    @Query("select * from classes where className = :classesName")
    fun getAllClasses(classesName: String) : List<Classes>
}