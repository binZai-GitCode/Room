package com.youwei.room.dao

import androidx.room.*
import com.youwei.room.entity.Classes
import com.youwei.room.entity.Students

/**
 * @Author：Bin YouWei
 * @Date：2022/6/27/11:51
 * @Declaration：用于实现Students表的查询
 */
@Dao
interface StudentsDao {

    /**
     * 增加一个学生
     */
    @Insert
    fun addStudent(students: Students)

    /**
     * 增加多个学生
     */
    @Insert
    fun addMultipleStudent(vararg students: Students)

    @Delete
    fun deleteStudent(students: Students)

    @Update
    fun updateStudent(students: Students)

    /**
     * 查询students表的全部数据
     */
    @Query("select * from students where sex = ':str'")
    fun getAllStudents(): List<Students>

}