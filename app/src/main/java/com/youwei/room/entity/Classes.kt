package com.youwei.room.entity

import androidx.room.AutoMigration
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @Author：Bin YouWei
 * @Date：2022/6/27/13:47
 * @Declaration：班级实体类
 */
@Entity
data class Classes(
    @PrimaryKey(autoGenerate = true) val classesId: Int = 0,
    @ColumnInfo(name = "className") var classesName: String,
    // 新增的数据表i
    @ColumnInfo(name = "classesNum") var classesNum: String
)