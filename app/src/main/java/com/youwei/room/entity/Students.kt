package com.youwei.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @Author：Bin YouWei
 * @Date：2022/6/27/13:46
 * @Declaration：学生实体类
 */
@Entity(tableName = "students")
data class Students(
    @PrimaryKey val studentId: Int,
    @ColumnInfo(name = "name") val studentName: String,
    @ColumnInfo(name = "sex") val sex: String,
    @ColumnInfo(name = "hight") val height: Int,
    @ColumnInfo(name = "admissionTime") val admissionTime: String,
    @ColumnInfo(name = "classesId") val classesId: Int
)